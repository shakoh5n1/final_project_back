const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/images'});


const zoomer = require('./routes/zoomer.route');
const app = express();

const mongoose = require('mongoose');

mongoose.connect('mongodb://134.209.29.20/zoomerDB', {useNewUrlParser: true});

mongoose.Promise = global.Promise;

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/zoomer', zoomer);
app.use(express.static('public'));

app.listen(5000, () => {
    console.log('listens')
});





