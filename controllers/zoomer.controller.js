const Product = require('../models/zoomer.model');

exports.addProduct = function (req, res) {
    let productList = new Product(
        {
            title: req.body.title,
            img: req.body.img,
            price: req.body.price,
            company: req.body.company,
            info: req.body.info,
            inCart: req.body.inCart,
            count: req.body.count,
            total: req.body.total
        }
    );

    productList.save(function(err) {
        if(err) {
            return next(err);
        }
        res.send({
            message : "Product add",
            status : 'ok'
        });
    });


};


exports.getProductList = function (req, res) {
    Product.find(req.find, function (err, productList) {
        if (err) return next(err);
        res.send(productList)
    })
}

exports.getDetailProduct = function (req, res) {
    Product.findById({ '_id' : req.params.productId }, function (err, Product) {
        if (err) res.send(err)
        res.send({
            message : 'Updated',
            status: 'ok'
        });
    });
}