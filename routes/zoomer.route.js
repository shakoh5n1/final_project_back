const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

const multer = require('multer');
const upload = multer({dest: __dirname + '/uploads/images'});

router.use(bodyParser.json())

const zoomerController = require('../controllers/zoomer.controller');

router.post('/addproduct', zoomerController.addProduct);

router.get('/productlist', zoomerController.getProductList);

router.get('/details/:productId' , zoomerController.getDetailProduct)

router.post('/upload', upload.single('photo'), (req, res) => {
    if(req.file) {
        res.json(req.file);
    }
    else throw 'error';
});

// router.get('/',)
module.exports = router;
