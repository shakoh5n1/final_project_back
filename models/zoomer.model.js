const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ProductSchema = new Schema({
    title: String,
    img: String,
    price: Number,
    company: String,
    info: String,
    inCart: Boolean,
    count: Number,
    total: Number
},
{
    collation: 'productList'
})

module.exports = mongoose.model('productList', ProductSchema )